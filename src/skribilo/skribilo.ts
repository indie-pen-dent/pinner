export class Skribilo {
    #apiKey: string;

    constructor(apiKey: string) {
        this.#apiKey = apiKey;
    }

    async updateHash(blogName: string, newHash: string) {
        const oldHash = await this.#getOldHash(blogName);

        if (oldHash === newHash) {
            return oldHash;
        }

        await this.#putNewHash(blogName, newHash);

        return oldHash;
    }

    async #getOldHash(blogName: string): Promise<string | undefined> {
        const response = await fetch(
            `https://www.skribilo.blog/api/hosted-blogs/${blogName}`,
            {
                method: 'GET',
                headers: {
                    authorization: `Bearer ${this.#apiKey}`,
                },
            }
        );

        if (response.status === 404) {
            return undefined;
        }

        if (!response.ok) {
            throw new Error('Error getting pinned data from Skribilo');
        }

        const body = await response.json();

        return body.pinnedHash;
    }

    async #putNewHash(blogName: string, newHash: string) {
        const response = await fetch(
            `https://www.skribilo.blog/api/hosted-blogs/${blogName}`,
            {
                method: 'PUT',
                headers: {
                    authorization: `Bearer ${this.#apiKey}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    pinnedHash: newHash,
                }),
            }
        );

        if (!response.ok) {
            throw new Error('Error putting hosted blog');
        }
    }
}

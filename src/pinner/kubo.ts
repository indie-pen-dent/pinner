import { toBase64 } from '../helpers/string.js';
import type { Pinner, FileReader } from './types';
import type { KuboConfig } from '../types';

export class Kubo implements Pinner {
    #authString: string;
    #fileReader: FileReader;

    constructor(config: KuboConfig, fileReader: FileReader) {
        this.#authString = this.#createAuthString(
            config.username,
            config.password
        );

        this.#fileReader = fileReader;
    }

    async add(directory: string) {
        const files = await this.#fileReader.read(directory);

        const response = await fetch(
            'https://ipfs.srchetwynd.co.uk/api/v0/add',
            {
                method: 'POST',
                headers: {
                    Authorization: this.#authString,
                },
                body: files,
            }
        );

        if (response.status !== 200) {
            throw new Error('IPFS server returned a none 200 status');
        }

        const responseBody = await response.text();
        const responseLines = responseBody.split('\n');

        return responseLines
            .filter((line) => line.length > 0)
            .map((line) => JSON.parse(line))
            .find(({ Name }) => Name.split('/').length === 1).Hash;
    }

    #createAuthString(username: string, password: string) {
        const rawAuthString = `${username}:${password}`;

        return toBase64(rawAuthString);
    }

    async remove(_hash: string) {
        return;
    }
}

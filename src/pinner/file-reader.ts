import fs from 'node:fs/promises';
import path from 'node:path';
import process from 'node:process';
import type { FileReader } from './types';

export class DirectoryFileReader implements FileReader {
    private files: FormData;
    private includeDirectories: boolean;
    private cwd = process.cwd();

    constructor(includeDirectories: boolean) {
        this.files = new FormData();
        this.includeDirectories = includeDirectories;
    }

    async read(directoryPath: string): Promise<FormData> {
        if (!this.isDirectory(directoryPath)) {
            throw new Error('Top path must be a directory');
        }

        this.addDirectory(this.fileNameCleaner(directoryPath));

        await this.collectFilesFromDirectory(directoryPath);

        return this.files;
    }

    private async collectFilesFromDirectory(
        directoryPath: string
    ): Promise<void> {
        const files = await this.getFilesFromDirectory(directoryPath);

        for (const file of files) {
            const filename = this.fileNameCleaner(file);

            if (await this.isDirectory(file)) {
                this.addDirectory(filename);
            } else {
                await this.addFile(file, filename);
            }
        }
    }

    private async addFile(file: string, filename: string) {
        const fileData = await fs.readFile(file);

        const fileBlob = new Blob([fileData], {
            type: 'text/html',
        });

        this.files.append('file', fileBlob, filename);
    }

    private addDirectory(path: string) {
        if (!this.includeDirectories) {
            return;
        }

        const fileBlob = new Blob([], { type: 'application/x-directory' });

        this.files.append('file', fileBlob, path);
    }

    private async getFilesFromDirectory(
        directoryPath: string
    ): Promise<string[]> {
        const files = await fs.readdir(directoryPath);

        const filePaths = [];

        for (const file of files) {
            const fullPath = path.join(directoryPath, file);

            const isDirectory = await this.isDirectory(fullPath);

            filePaths.push(fullPath);

            if (isDirectory) {
                filePaths.push(await this.getFilesFromDirectory(fullPath));
            }
        }

        return filePaths.flat();
    }

    private async isDirectory(filePath: string): Promise<boolean> {
        const stats = await fs.stat(filePath);

        return stats.isDirectory();
    }

    private fileNameCleaner(fileName: string): string {
        return fileName.replace(this.cwd, '');
    }
}

export function toBase64(content: string) {
    const stringBuffer = Buffer.from(content);

    return stringBuffer.toString('base64');
}

interface CloudflareConfig {
    apiToken: string;
    apiEmail: string;
    zoneId: string;
}

export class Cloudflare {
    #apiUrl = 'https://api.cloudflare.com/client/v4';
    #config: CloudflareConfig;

    constructor(config: CloudflareConfig) {
        this.#config = config;
    }

    async updateDNSLink(hostnameId: string, ipfsAddress: string) {
        const url = this.#createUpdateUrl(hostnameId);

        const body = {
            dnslink: ipfsAddress,
        };

        const result = await fetch(url, {
            method: 'PATCH',
            body: JSON.stringify(body),
            headers: {
                'X-Auth-Key': this.#config.apiToken,
                'X-Auth-Email': this.#config.apiEmail,
                'Content-Type': 'application/json',
            },
        });

        if (!result.ok) {
            console.log(await result.text());
            throw new Error('Cloudflare returned a none ok status');
        }
    }

    #createUpdateUrl(hostnameId: string) {
        return `${this.#apiUrl}/zones/${
            this.#config.zoneId
        }/web3/hostnames/${hostnameId}`;
    }
}

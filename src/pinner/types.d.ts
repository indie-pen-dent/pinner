export interface Pinner {
    add: (directory: string) => Promise<string>;
    remove: (hash: string) => Promise<void>;
}

export interface FileReader {
    read: (directory: string) => Promise<FormData>;
}

import { Kubo } from './kubo.js';
import { DirectoryFileReader } from './file-reader.js';
import { Pinata } from './pinata.js';
import type { Config } from '../types';

export class PinnerContainer {
    #config: Config;

    constructor(config: Config) {
        this.#config = config;
    }

    getPinner() {
        if (this.#config.pinningService === 'kubo') {
            return this.#getKuboPinner();
        }

        if (this.#config.pinningService === 'pinata') {
            return this.#getPinataPinner();
        }

        throw new Error('Invalid Pinner.');
    }

    #getKuboPinner() {
        if (!this.#config.kubo) {
            throw new Error('No kubo config.');
        }

        return new Kubo(this.#config.kubo, this.#getFileReader(true));
    }

    #getFileReader(includeDirectories: boolean) {
        return new DirectoryFileReader(includeDirectories);
    }

    #getPinataPinner() {
        if (!this.#config.pinata) {
            throw new Error('No pinata config.');
        }

        return new Pinata(this.#config.pinata, this.#getFileReader(false));
    }
}

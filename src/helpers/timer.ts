export class Timer {
    #name: string;
    #startTime?: number;

    constructor(name: string) {
        this.#name = name;
    }

    start(): Timer {
        console.log(`Starting: ${this.#name}`);

        this.#startTime = Date.now();

        return this;
    }

    end(): Timer {
        if (!this.#startTime) {
            throw new Error(`${this.#name} timer not started.`);
        }

        const currentTime = Date.now();

        const duration = currentTime - this.#startTime;

        console.log(`Finished: ${this.#name}, time elapsed: ${duration}.`);

        return this;
    }
}

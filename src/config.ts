import process from 'node:process';
import 'dotenv/config';
import type {
    KuboConfig,
    CloudflareConfig,
    Config,
    PinningService,
    PinataConfig,
} from './types';

if (!process.env.PINNING_SERVICE) {
    throw new Error('PINNING_SERVICE environment variable is required.');
}

if (!isValidPinningService(process.env.PINNING_SERVICE)) {
    throw new Error('Invalid PINNING_SERVICE.');
}

export const pinningService: PinningService = process.env.PINNING_SERVICE;

if (pinningService === 'kubo') {
    if (!process.env.KUBO_PASSWORD) {
        throw new Error('KUBO_PASSWORD environment variable is required.');
    }

    if (!process.env.KUBO_USERNAME) {
        throw new Error('KUBO_USERNAME environment variable is required.');
    }
}

if (pinningService === 'pinata' && !process.env.PINATA_JWT) {
    throw new Error('PINATA_JWT environment variable is required.');
}

if (!process.env.CLOUDFLARE_API_KEY) {
    throw new Error('CLOUDFLARE_API_KEY environment variable is required.');
}

if (!process.env.CLOUDFLARE_ZONE_ID) {
    throw new Error('CLOUDFLARE_ZONE_ID environment variable is required.');
}

if (!process.env.CLOUDFLARE_EMAIL) {
    throw new Error('CLOUDFLARE_EMAIL environment variable is required.');
}

if (!process.env.CLOUDFLARE_HOSTNAME_ID) {
    throw new Error('CLOUDFLARE_HOSTNAME_ID environment variable is required.');
}

if (!process.env.SKRIBILO_API_KEY) {
    throw new Error('SKRIBILO_API_KEY environment variable is required.');
}

if (!process.env.BLOG_NAME) {
    throw new Error('BLOG_NAME environment variable is required.');
}

const kubo: KuboConfig = {
    password: process.env.KUBO_PASSWORD || '',
    username: process.env.KUBO_USERNAME || '',
};

const pinata: PinataConfig = {
    jwt: process.env.PINATA_JWT || '',
};

const clouldflare: CloudflareConfig = {
    apiKey: process.env.CLOUDFLARE_API_KEY,
    zoneId: process.env.CLOUDFLARE_ZONE_ID,
    email: process.env.CLOUDFLARE_EMAIL,
    hostnameId: process.env.CLOUDFLARE_HOSTNAME_ID,
};

export const config: Config = {
    kubo: pinningService === 'kubo' ? kubo : undefined,
    pinata: pinningService === 'pinata' ? pinata : undefined,
    skribilo: {
        apiKey: process.env.SKRIBILO_API_KEY,
    },
    clouldflare,
    pinningService,
    blogName: process.env.BLOG_NAME,
};

function isValidPinningService(service: string): service is PinningService {
    return ['kubo', 'pinata'].includes(service);
}

#!/usr/bin/env node

import { config } from './config.js';
import path from 'node:path';
import fs from 'node:fs/promises';
import process from 'node:process';
import { PinnerContainer } from './pinner/container.js';
import { Cloudflare } from './dns-updater/cloudflare.js';
import { Hugo } from './blog-builder/hugo.js';
import { Timer } from './helpers/timer.js';
import { Skribilo } from './skribilo/skribilo.js';

const BUILD_DIR = 'public';
const pathToPublish = path.join(process.cwd(), `./${config.blogName}`) + '/';

await buildBlog();

await fs.rename(BUILD_DIR, config.blogName);

const directoryHash = await pinBlog();

await updateDNS(directoryHash);

await unpinOldData(directoryHash);

async function buildBlog() {
    const buildTimer = new Timer('Blog Build').start();
    const hugo = new Hugo();

    await hugo.build();
    buildTimer.end();
}

async function pinBlog() {
    const pinningTimer = new Timer('Pinning').start();
    const pinnerContainer = new PinnerContainer(config);

    const pinner = pinnerContainer.getPinner();

    const directoryHash = await pinner.add(pathToPublish);
    pinningTimer.end();

    return directoryHash;
}

async function updateDNS(hash: string) {
    const dnsTimer = new Timer('DNS Update').start();

    const cloudflare = new Cloudflare({
        zoneId: config.clouldflare.zoneId,
        apiToken: config.clouldflare.apiKey,
        apiEmail: config.clouldflare.email,
    });

    await cloudflare.updateDNSLink(
        config.clouldflare.hostnameId,
        `/ipfs/${hash}`
    );

    dnsTimer.end();
}

async function unpinOldData(newHash: string) {
    const unpinTimer = new Timer('Unpinning').start();

    const skribilo = new Skribilo(config.skribilo.apiKey);

    const oldHash = await skribilo.updateHash(config.blogName, newHash);

    if (newHash === oldHash) {
        unpinTimer.end();
        return;
    }

    const pinnerContainer = new PinnerContainer(config);
    const pinner = pinnerContainer.getPinner();

    if (oldHash !== undefined) {
        pinner.remove(oldHash);
    }

    unpinTimer.end();
}

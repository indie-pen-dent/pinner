import type { BlogBuilder } from './types';
import { execFile } from 'node:child_process';
import hugoPath from 'hugo-bin';

export class Hugo implements BlogBuilder {
    build() {
        return new Promise<boolean>((resolve, reject) => {
            execFile(hugoPath, [], (error: unknown) => {
                if (error) {
                    return reject(error);
                }

                return resolve(true);
            });
        });
    }
}

export interface BlogBuilder {
    build: () => Promise<boolean>;
}

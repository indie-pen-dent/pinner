FROM node:slim
WORKDIR /app

COPY ./tsconfig.json ./tsconfig.json

COPY ./package.json ./package.json
RUN npm i

COPY ./src ./src
RUN npm run build

RUN npm link

WORKDIR /blog

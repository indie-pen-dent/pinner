export interface KuboConfig {
    username: string;
    password: string;
}

export interface CloudflareConfig {
    apiKey: string;
    zoneId: string;
    email: string;
    hostnameId: string;
}

export interface PinataConfig {
    jwt: string;
}

export interface SkribiloConfig {
    apiKey: string;
}

export type PinningService = 'kubo' | 'pinata';

export interface Config {
    clouldflare: CloudflareConfig;
    kubo?: KuboConfig;
    pinata?: PinataConfig;
    skribilo: SkribiloConfig;
    pinningService: PinningService;
    blogName: string;
}

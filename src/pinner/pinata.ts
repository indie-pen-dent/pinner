import type { Pinner, FileReader } from './types';
import type { PinataConfig } from '../types';

export class Pinata implements Pinner {
    #fileReader: FileReader;
    #pinataJWT: string;

    constructor(config: PinataConfig, fileReader: FileReader) {
        this.#fileReader = fileReader;
        this.#pinataJWT = config.jwt;
    }

    async add(directory: string) {
        const files = await this.#fileReader.read(directory);

        const response = await fetch(
            'https://api.pinata.cloud/pinning/pinFileToIPFS',
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${this.#pinataJWT}`,
                },
                body: files,
            }
        );

        if (!response.ok) {
            throw new Error('Pinning Unsuccessful.');
        }

        const content = await response.json();

        return content.IpfsHash;
    }

    async remove(hash: string) {
        const response = await fetch(
            `https://api.pinata.cloud/pinning/unpin/${hash}`,
            {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${this.#pinataJWT}`,
                },
            }
        );

        if (!response.ok) {
            throw new Error('Unpinning Unsuccessful.');
        }
    }
}
